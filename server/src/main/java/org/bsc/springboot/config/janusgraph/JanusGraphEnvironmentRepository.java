package org.bsc.springboot.config.janusgraph;

import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.configuration.Configuration;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.__;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.structure.Vertex;
import org.apache.tinkerpop.gremlin.structure.util.GraphFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.config.environment.Environment;
import org.springframework.cloud.config.environment.PropertySource;
import org.springframework.cloud.config.server.environment.EnvironmentRepository;

/**
 * 
 * @author bsorrentino
 *
 */
public class JanusGraphEnvironmentRepository implements EnvironmentRepository {
    private static final String PROFILE_ATTRIBUTE = "profile";

    private static final String VERSION_ATTRIBUTE = "version";

    private static final String BELONG_TO_LABEL = "belong_to";

    private static final String CONFIG_LABEL = "configuration";

    private static final Logger LOGGER = LoggerFactory.getLogger(JanusGraphEnvironmentRepository.class);
    
    @Autowired
    private JanusgraphComponent janusgraph;
    
    
    /**
     * 
     * @param janusConfigUrl
     */
    public JanusGraphEnvironmentRepository() {
    }

    /**
     * load test data
     */
    @PostConstruct
    private void loadTestData() {
        janusgraph.submitAndCommit( this::loadTestData );
    }
    

    /**
     * 
     * @param application
     * @param profile
     * @param label
     * @return
     */
    PropertySource getPropertySource(String application, String profile, String label) {
        LOGGER.info( "===> JanusGraphEnvironmentRepository.getPropertySource( application:'{}', profile:'{}', label:'{}' )",
                application,
                profile,
                label
            );
        
        
        final java.util.Map<?,?> source = janusgraph.submit( g -> 
            g.V()
            .hasLabel(CONFIG_LABEL)
            .inE( BELONG_TO_LABEL)
            .has(PROFILE_ATTRIBUTE, profile)
            .outV()
            .hasLabel(application)
            .has(VERSION_ATTRIBUTE,label)
            .valueMap("name", "value")
            .toStream()
            .peek( System.out::println )
            //.collect( Collectors.toMap( p -> p.get("name"), p -> getSingleValue(p.get("value") )));
            .collect( Collectors.toMap( p -> p.get("name"), p -> p.get("value") )) )
            .orElse( Collections.emptyMap() );
        
        
        return new PropertySource( String.format("%s-%s", application,profile), source);
        
    }

    /**
     * return Environment containing all the configuration attributes related to (application,profile,label)
     */
    @Override
    public Environment findOne(String application, String profile, String label) {
        Objects.requireNonNull(application, "application is null!");

        LOGGER.info( "===> JanusGraphEnvironmentRepository.findOne( application:'{}', profile:'{}', label:'{}' )",
                    application,
                    profile,
                    label
                );
        
        final String[] profiles = 
                (Optional.ofNullable(profile).orElse( "default" ))
                .split(",");
                    
        final Environment result = new Environment( application, profiles );
        
        result.setLabel( Optional.ofNullable(label).orElse("master") );
        
        for( String p : profiles ) {
            
            final PropertySource ps = getPropertySource(result.getName(), p, result.getLabel());
            
            result.add( ps );
             
        }
        return result;
    }

    
    
    @SuppressWarnings("unchecked")
    private Vertex addConfigPoint( 
            Vertex root, 
            String name,
            Object value,
            String application,
            String profile, 
            String label )
    {
        
        return janusgraph.submit( g -> 
                g.V(root.id())
                .inE( BELONG_TO_LABEL )
                .has(PROFILE_ATTRIBUTE, profile )
                .outV()
                .hasLabel(application)
                .has("name", name)
                .has(VERSION_ATTRIBUTE, label)
                .fold()
                .coalesce( __.unfold(), 
                           __.V(root.id())
                            .as("root")
                            .addV(application)
                            .property("name", name)
                            .property(VERSION_ATTRIBUTE, label)
                            .property( "value", value )
                            .addE( BELONG_TO_LABEL)
                            .property(PROFILE_ATTRIBUTE, profile)
                            .to( "root" )
                            .outV()
                )
                .next())
                .get()
                ;
    }
    
    private void loadTestData( GraphTraversalSource g ) {
    
        @SuppressWarnings("unchecked")
        final Vertex root = 
            g.V().hasLabel(CONFIG_LABEL)
                .fold()
                .coalesce( __.unfold(), __.addV(CONFIG_LABEL) )
                .next()                
                ;
        
        String app = "client";
        
        String profile = "default";
        String label = "master";
        
        addConfigPoint( root, "welcome", "janusgraph", app, profile, label);
        addConfigPoint( root, "isbar", false, app, profile, label);

        addConfigPoint( root, "p1", "v1", app, profile, label);
        addConfigPoint( root, "p2", "v2", app, profile, label);
        addConfigPoint( root, "p3", "v3", app, profile, label);

        getPropertySource( app, profile, label );

        profile = "prod";
        
        addConfigPoint( root, "p1", "v10", app, profile, label);
        addConfigPoint( root, "p2", "v20", app, profile, label);
        addConfigPoint( root, "p3", "v30", app, profile, label);
        
        LOGGER.info( "==> # Config Points {}", g.V().count().next() );
        LOGGER.info( "==> # Config Points {}", g.V(root.id()).inE().has(PROFILE_ATTRIBUTE, "default").count().tryNext().orElse(0L) );
        
        getPropertySource( app, profile, label );
   
    }
}
