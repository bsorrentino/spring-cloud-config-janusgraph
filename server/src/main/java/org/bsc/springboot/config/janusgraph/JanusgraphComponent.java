package org.bsc.springboot.config.janusgraph;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.tinkerpop.gremlin.process.traversal.dsl.graph.GraphTraversalSource;
import org.apache.tinkerpop.gremlin.structure.Graph;
import org.apache.tinkerpop.gremlin.structure.util.GraphFactory;
import org.apache.tinkerpop.gremlin.structure.util.TransactionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * 
 * @author bsorrentino
 *
 */
@Component
public class JanusgraphComponent {
    private static final Logger LOGGER = LoggerFactory.getLogger(JanusgraphComponent.class);

    @Value( "${janus.config.url:classpath:/janus-cql.properties}")
    String janusConfigUrl;

    
    private Graph graph;
    protected Optional<GraphTraversalSource> traversal = Optional.empty();
    
    /**
     * 
     * @return
     */
    public GraphTraversalSource getTraversal() {
        return traversal.get();
    }
    
    /**
     * 
     * @return
     */
    public Optional<GraphTraversalSource> optTraversal() {
        return traversal;
    }
    
    
    @PostConstruct
    void openGraph() {
        LOGGER.info("opening graph");
        try {
            final Configuration config = new PropertiesConfiguration(new URL(janusConfigUrl));

            graph = GraphFactory.open(config);
            traversal = Optional.ofNullable(graph.traversal());
        }
        catch (MalformedURLException e) {
            final String msg = 
                    String.format("janusConfigUrl [%s] is not valid!", janusConfigUrl);
            LOGGER.error( msg, e);
            //throw new Error( msg, e );               
        } catch (ConfigurationException e) {
            final String msg = 
                    String.format("reading conf in '%s' is not valid!", janusConfigUrl);
            LOGGER.error( msg, e);
            //throw new Error( msg, e );
        }    
        catch (Throwable e) {
            LOGGER.error( "error opening graph", e);
        }
        
    }
    
    @PreDestroy
    void closeGraph() {
        LOGGER.info("closing graph");

        traversal.ifPresent( this::close );
        close( graph );

        traversal = Optional.empty();
        graph = null;
        
    }
    
    /**
     * 
     * @param action
     * @return
     */
    public <R> Optional<R> submit( java.util.function.Function<GraphTraversalSource,R> action ) {
        
        return optTraversal()
                .flatMap( g -> Optional.ofNullable(action.apply(g)) )
        ;
    }
    
    /**
     * 
     * @param action
     * @return
     */
    public void submitAndCommit( java.util.function.Consumer<GraphTraversalSource> action ) {        
        optTraversal().ifPresent( g -> submitAndCommit( g, action )) ;
    }

    
    private void submitAndCommit( GraphTraversalSource g, java.util.function.Consumer<GraphTraversalSource> action ) {
        try {         
            action.accept(g);
            
            debugCommit(g);
        }
        catch( Throwable e) {
            
            safeRollback( g );
        }
        
    }
    
    private void close( GraphTraversalSource g ) {
        try {
            g.close();
        } catch (Exception e) {
            LOGGER.warn( "error closing traversal", e);
        }
    }
    
    private void close( Graph graph ) {
        try {
            graph.close();
        } catch (Exception e) {
            LOGGER.warn( "error closing graph", e);
        }
    }
    
    private void debugCommit( GraphTraversalSource g ) {
        try {
            g.tx().commit();
        }
        catch( TransactionException e ) {
            LOGGER.error( "Janusgraph commit exception", e);
            throw e;
        }
    }
    
    private void safeRollback( GraphTraversalSource g ) {
        
        try {
            g.tx().rollback();
        }
        catch( TransactionException e ) {
            LOGGER.warn( "Janusgraph rollback exception", e);
        }
    }
}
