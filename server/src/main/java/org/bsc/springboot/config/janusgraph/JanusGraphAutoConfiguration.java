package org.bsc.springboot.config.janusgraph;

import java.net.MalformedURLException;
import java.net.URL;

import javax.annotation.PostConstruct;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * 
 * @author bsorrentino
 *
 */
@Configuration
@Profile("janus")
public class JanusGraphAutoConfiguration {
    private static final Logger LOGGER = LoggerFactory.getLogger(JanusGraphAutoConfiguration.class);
    
    @PostConstruct
    public void initialize() {
        LOGGER.info( "===> JanusGraphAutoConfiguration constructed");
    }
    
    @Bean
    @ConditionalOnMissingBean(JanusGraphEnvironmentRepository.class)
    public JanusGraphEnvironmentRepository tinkerPopEnvironmentRepository()
    {
            return new JanusGraphEnvironmentRepository();
    }

}
