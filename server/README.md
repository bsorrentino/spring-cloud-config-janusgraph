## Spring Cloud Config Server implementation based upon Janusgraph


### Test Using Docker Compose

Compose from [Datastax](https://github.com/datastax/docker-images/tree/master/example_compose_yamls)

```yaml
version: '2'
services:
  seed_node:
    image: "datastax/dse-server"
    environment:
      - DS_LICENSE=accept
    # Allow DSE to lock memory with mlock
    cap_add:
    - IPC_LOCK
    ulimits:
      memlock: -1
    ports:
      - 9042:9042
  node:
    image: "datastax/dse-server"
    environment:
      - DS_LICENSE=accept
      - SEEDS=seed_node
    links:
      - seed_node
    # Allow DSE to lock memory with mlock
    cap_add:
    - IPC_LOCK
    ulimits:
      memlock: -1

```


Start Cassandra with 2 Node(s)

```
docker-compose  -f docker-compose.yml up -d --scale node=1
```

Start Server using Maven


```
mvn clean package
mvn docker:build
mvn docker:run
```

